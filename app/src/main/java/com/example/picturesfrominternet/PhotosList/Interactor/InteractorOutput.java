package com.example.picturesfrominternet.PhotosList.Interactor;

import java.util.ArrayList;

public interface InteractorOutput {
    void interactorDidLoadPhotos(ArrayList<PhotoViewItem> items);

    void interactorDidLoadPhotosForRestore(ArrayList<PhotoViewItem> items);

    void interactorDidLoadPhotosFromDataBase(ArrayList<PhotoViewItem> items, String text);
}
