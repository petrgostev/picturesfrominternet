package com.example.picturesfrominternet.PhotosList.Interactor;

public interface InteractorInput {
    void loadPhotos(String text);

    void loadPhotosForRestore();

    void loadMoreItems();

    void loadPhotosFromDataBase();
}
