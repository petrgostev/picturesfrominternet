package com.example.picturesfrominternet.PhotosList.Interactor;

import io.realm.RealmObject;


public class PhotoViewItem extends RealmObject {
    public String url;
    public int width;
    public int height;
}
