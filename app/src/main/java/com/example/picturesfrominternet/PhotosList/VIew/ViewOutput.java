package com.example.picturesfrominternet.PhotosList.VIew;


public interface ViewOutput {
    void viewDidTapSearchButton(String text);

    void viewDidRestoreInstanceState();

    void viewNeedsMoreItems();

    void viewDidConfigureModule();
}
