package com.example.picturesfrominternet.PhotosList.VIew;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;
import com.example.picturesfrominternet.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class PhotosListAdapter extends RecyclerView.Adapter<PhotosListAdapter.ViewHolder> {

    private List<PhotoViewItem> viewItems = new ArrayList<>();

    Context context;

//    PhotosListAdapter.Listener selectListener;

    public PhotosListAdapter(Context context) {
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int res = R.layout.photo_card;

        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(res, parent, false);

        ViewHolder vh = new ViewHolder(cv);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosListAdapter.ViewHolder holder, int position) {
        CardView cardView = holder.cardView;

        configureView(cardView, position, holder);
    }

    @Override
    public int getItemCount() {
        return viewItems.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setViewItems(List<PhotoViewItem> viewItems) {
        this.viewItems = viewItems;
    }

    int getRes() {
        return R.layout.photo_card;
    }

    private void configureView(CardView cardView, int position, final ViewHolder holder) {
        PhotoViewItem viewItem = viewItems.get(position);

        final ImageView photoIV = cardView.findViewById(R.id.photo_image);

        String url = viewItem.url;

        int height = viewItem.height * 2;
        int width = viewItem.width * 2;

        photoIV.setMinimumHeight(height);
        photoIV.setMaxHeight(height);

        photoIV.setMinimumWidth(width);
        photoIV.setMaxWidth(width);

//        fileService.image(viewItem.src, new LoadImageFileCallback() {
//            @Override
//            public void imageFileDidLoad(File imageFile) {
//                if (imageFile != null) {
        Picasso.with(photoIV.getContext())
                .load(url)
                .resize(width, height)
//                            .centerCrop()
//                            .placeholder(R.drawable.spinner)
                .into(photoIV);
//                }
//            }
//        });

//        cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (selectListener != null) {
//                    selectListener.onClick(holder.getAdapterPosition());
//                }
//            }
//        });

    }


//    public interface Listener {
//        void onClick(int position);
//    }

//    public void setSelectListener(Listener selectListener) {
//        this.selectListener = selectListener;
//    }
}
