package com.example.picturesfrominternet.PhotosList.Assembly;

import com.example.picturesfrominternet.PhotoSevice.PhotoService;
import com.example.picturesfrominternet.PhotosList.Interactor.Interactor;
import com.example.picturesfrominternet.PhotosList.Presenter.Presenter;
import com.example.picturesfrominternet.PhotosList.VIew.MainActivity;


public class Assembly {

    public static void configureModule(MainActivity view) {
        PhotoService photoService = PhotoService.getInstance(view);
        Interactor interactor = new Interactor(photoService);
        Presenter presenter = new Presenter();

        view.setViewOutput(presenter);

//        presenter.setRouter(router);
        presenter.setInteractor(interactor);
        presenter.setViewInterface(view);

        interactor.setOutput(presenter);
    }
}
