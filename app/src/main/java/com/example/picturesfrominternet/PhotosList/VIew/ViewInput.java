package com.example.picturesfrominternet.PhotosList.VIew;

import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;

import java.util.ArrayList;


public interface ViewInput {
    void showPhotos(ArrayList<PhotoViewItem> items);

    void showPhotosForRestore(ArrayList<PhotoViewItem> items);

    void showPhotosFromDataBase(ArrayList<PhotoViewItem> items, String text);
}
