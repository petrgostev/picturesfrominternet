package com.example.picturesfrominternet.PhotosList.Presenter;

import com.example.picturesfrominternet.PhotosList.Interactor.InteractorInput;
import com.example.picturesfrominternet.PhotosList.Interactor.InteractorOutput;
import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;
import com.example.picturesfrominternet.PhotosList.VIew.ViewInput;
import com.example.picturesfrominternet.PhotosList.VIew.ViewOutput;

import java.util.ArrayList;


public class Presenter implements ViewOutput, InteractorOutput {

    InteractorInput interactor;
    ViewInput viewInterface;

    public void setInteractor(InteractorInput interactor) {
        this.interactor = interactor;
    }

    public void setViewInterface(ViewInput viewInterface) {
        this.viewInterface = viewInterface;
    }

    //ViewOutput

    @Override
    public void viewDidTapSearchButton(String text) {
        interactor.loadPhotos(text);
    }

    @Override
    public void viewDidRestoreInstanceState() {
        interactor.loadPhotosForRestore();
    }

    @Override
    public void viewNeedsMoreItems() {
        interactor.loadMoreItems();
    }

    @Override
    public void viewDidConfigureModule() {
        interactor.loadPhotosFromDataBase();
    }

    //InteractorOutput

    @Override
    public void interactorDidLoadPhotos(ArrayList<PhotoViewItem> items) {
        viewInterface.showPhotos(items);
    }

    @Override
    public void interactorDidLoadPhotosForRestore(ArrayList<PhotoViewItem> items) {
        viewInterface.showPhotosForRestore(items);
    }

    @Override
    public void interactorDidLoadPhotosFromDataBase(ArrayList<PhotoViewItem> items, String text) {
        viewInterface.showPhotosFromDataBase(items, text);
    }
}
