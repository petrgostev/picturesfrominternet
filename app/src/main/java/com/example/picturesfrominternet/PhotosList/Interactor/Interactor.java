package com.example.picturesfrominternet.PhotosList.Interactor;

import com.example.picturesfrominternet.PhotoSevice.PhotoCallback;
import com.example.picturesfrominternet.PhotoSevice.PhotoServiceInterface;

import java.util.ArrayList;
import java.util.List;


public class Interactor implements InteractorInput {

    InteractorOutput output;
    PhotoServiceInterface dataService;

    public Interactor(PhotoServiceInterface dataService) {
        this.dataService = dataService;
    }

    public void setOutput(InteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loadPhotos(String text) {

        dataService.getPhotosViewItems(text, new PhotoCallback() {
            @Override
            public void done(Error error, ArrayList<PhotoViewItem> items, String text) {
                if (error == null && !items.isEmpty()) {
                    output.interactorDidLoadPhotos(items);
                } else {
//                    Toast.makeText(context, "Нет данных", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void loadMoreItems() {
        dataService.getPhotosViewItems(null, (error, items, text) -> {
            if (error == null && !items.isEmpty()) {
                output.interactorDidLoadPhotos(items);
            } else {
//                    Toast.makeText(context, "Нет данных", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void loadPhotosForRestore() {
        output.interactorDidLoadPhotosForRestore(dataService.getItems());
    }

    @Override
    public void loadPhotosFromDataBase() {
        dataService.getPhotosFromDataBase(new PhotoCallback() {
            @Override
            public void done(Error error, ArrayList<PhotoViewItem> items, String text) {
                if (error == null) {
                    output.interactorDidLoadPhotosFromDataBase(items, text);
                }
            }
        });

    }
}
