package com.example.picturesfrominternet.PhotosList.VIew;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.MenuCompat;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.ComponentName;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.example.picturesfrominternet.PhotosList.Assembly.Assembly;
import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;
import com.example.picturesfrominternet.R;

import java.util.ArrayList;
import java.util.Objects;


public class MainActivity extends AppCompatActivity implements ViewInput {

    private PhotosListAdapter adapter;

    ArrayList<PhotoViewItem> items;

    private ViewOutput viewOutput;

    CoordinatorLayout coordinatorLayout;

    private RecyclerView photoRecycler;

    final static String FIRST_VISIBLE_ITEM_POSITION_KEY = "firstVisibleItemPosition";
    final static String TEXT_SEARCH_KEY = "textSearchKey";
    final static String SAVED_STATE_KEY = "savedStateKey";

    private int position = 0;
    private int spanCount = 2;
    private int sizeItems;
    private boolean savedState = false;
    private String textSearch = null;

    MenuItem searchItem;
    SearchView searchView;

    private StaggeredGridLayoutManager layoutManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        Assembly.configureModule(this);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        photoRecycler = findViewById(R.id.photo_recycler);

        adapter = new PhotosListAdapter(this);
        photoRecycler.setAdapter(adapter);

        layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);

        photoRecycler.setLayoutManager(layoutManager);

        photoRecycler.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int[] visibleItems = ((StaggeredGridLayoutManager) Objects.requireNonNull(photoRecycler.getLayoutManager()))
                    .findLastVisibleItemPositions(null);
            int lastItem = 0;

            for (int i : visibleItems) {
                lastItem = lastItem + i;
            }

            if (lastItem > 0 && lastItem > sizeItems - 5) {
                viewOutput.viewNeedsMoreItems();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!savedState) {
            viewOutput.viewDidConfigureModule();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        position = layoutManager.findFirstVisibleItemPositions(new int[spanCount])[0];
        savedState = true;

        savedInstanceState.putInt(FIRST_VISIBLE_ITEM_POSITION_KEY, position);
        savedInstanceState.putString(TEXT_SEARCH_KEY, textSearch);
        savedInstanceState.putBoolean(SAVED_STATE_KEY, savedState);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        position = savedInstanceState.getInt(FIRST_VISIBLE_ITEM_POSITION_KEY);
        textSearch = savedInstanceState.getString(TEXT_SEARCH_KEY);
        savedState = savedInstanceState.getBoolean(SAVED_STATE_KEY);

        viewOutput.viewDidRestoreInstanceState();
    }

    public void setViewOutput(ViewOutput viewOutput) {
        this.viewOutput = viewOutput;
    }

    @Override
    public void showPhotos(ArrayList<PhotoViewItem> items) {
        this.items = items;
        sizeItems = items.size();

        adapter.setViewItems(items);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void showPhotosForRestore(ArrayList<PhotoViewItem> items) {
        this.items = items;
        sizeItems = items.size();

        adapter.setViewItems(items);
//        adapter.notifyDataSetChanged();
        layoutManager.scrollToPosition(position);

    }

    @Override
    public void showPhotosFromDataBase(ArrayList<PhotoViewItem> items, String text) {
        this.items = items;
        sizeItems = 20;

        if (text != null) {
            textSearch = text;
        }

        adapter.setViewItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchItem.getActionView();

        if (textSearch != null) {
            searchView.setIconifiedByDefault(false);
            searchView.setQuery(textSearch, false);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                viewOutput.viewDidTapSearchButton(query);
                textSearch = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }
}
