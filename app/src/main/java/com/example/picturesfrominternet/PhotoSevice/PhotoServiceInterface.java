package com.example.picturesfrominternet.PhotoSevice;

import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;

import java.util.ArrayList;


public interface PhotoServiceInterface {
    void getPhotosViewItems(String text, PhotoCallback callback);

    ArrayList<PhotoViewItem> getItems();

    void getPhotosFromDataBase(PhotoCallback callback);
}
