package com.example.picturesfrominternet.PhotoSevice;

import com.example.picturesfrominternet.PhotoSevice.Objects.PhotoResponse;
import com.google.gson.annotations.SerializedName;


public class ObjectsResult {
    @SerializedName("response")
    public PhotoResponse result;
}
