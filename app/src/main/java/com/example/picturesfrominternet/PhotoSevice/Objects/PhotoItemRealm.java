package com.example.picturesfrominternet.PhotoSevice.Objects;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;


public class PhotoItemRealm extends RealmObject {
    private String text;

    private RealmList<PhotoItem> photoItems;

    public String getText() {
        return text;
    }

    public RealmList<PhotoItem> getPhotoItems() {
        return photoItems;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPhotoItems(RealmList<PhotoItem> photoItems) {
        this.photoItems = photoItems;
    }
}
