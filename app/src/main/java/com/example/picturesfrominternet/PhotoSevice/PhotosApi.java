package com.example.picturesfrominternet.PhotoSevice;

import com.example.picturesfrominternet.PhotoSevice.Objects.PhotoResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface PhotosApi {

    @GET("method/photos.search?count=10&access_token=37444421ec3d3aa929e39fb4981a20c3e1bac49eef81ab0814f87c2fe624a8e6b02aff4b849fad20ea28c&v=5.95")
    Call<ObjectsResult> listPhotos(@Query("q") String name,
                                   @Query("offset") int countOffSet);
}
