package com.example.picturesfrominternet.PhotoSevice.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class PhotoItem extends RealmObject {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("album_id")
    @Expose
    private Integer albumId;

    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;

    @SerializedName("sizes")
    @Expose
    private RealmList<PhotoSize> sizes;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("date")
    @Expose
    private Integer date;

    @SerializedName("post_id")
    @Expose
    private Integer postId;

//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getAlbumId() {
//        return albumId;
//    }
//
//    public void setAlbumId(Integer albumId) {
//        this.albumId = albumId;
//    }
//
//    public Integer getOwnerId() {
//        return ownerId;
//    }
//
//    public void setOwnerId(Integer ownerId) {
//        this.ownerId = ownerId;
//    }

    public RealmList<PhotoSize> getSizes() {
        return sizes;
    }

//    public void setSizes(List<PhotoSize> sizes) {
//        this.sizes = sizes;
//    }
//
//    public String getText() {
//        return text;
//    }
//
//    public void setText(String text) {
//        this.text = text;
//    }
//
//    public Integer getDate() {
//        return date;
//    }
//
//    public void setDate(Integer date) {
//        this.date = date;
//    }
//
//    public Integer getPostId() {
//        return postId;
//    }
//
//    public void setPostId(Integer postId) {
//        this.postId = postId;
//    }

    public String getUrl() {
        for (PhotoSize size : getSizes()) {
            if (size.getType().equals("q")) {
                return size.getUrl();
            }
        }
        return null;
    }

    public int getWidth() {
        for (PhotoSize size : getSizes()) {
            if (size.getType().equals("q")) {
                return size.getWidth();
            }
        }
        return 0;
    }

    public int getHeight() {
        for (PhotoSize size : getSizes()) {
            if (size.getType().equals("q")) {
                return size.getHeight();
            }
        }
        return 0;
    }

}
