package com.example.picturesfrominternet.PhotoSevice;

import android.content.Context;
import android.util.Log;

import com.example.picturesfrominternet.PhotoSevice.Objects.PhotoItem;
import com.example.picturesfrominternet.PhotoSevice.Objects.PhotoItemRealm;
import com.example.picturesfrominternet.PhotoSevice.Objects.PhotoResponse;
import com.example.picturesfrominternet.PhotoSevice.Objects.PhotoSize;
import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PhotoService implements PhotoServiceInterface {

    private static PhotoService ourInstance;

    private Realm realm;

    private PhotoResponse photoResponse;

    private ArrayList<PhotoViewItem> items;

    private Context context;

    private String searchText;

    private PhotoService(Context context) {
        this.context = context;
        Realm.init(context);
        realm = Realm.getDefaultInstance();
        searchText = null;
    }

    public static PhotoService getInstance(Context context) {

        if (ourInstance == null) {
            ourInstance = new PhotoService(context);
        }

        return ourInstance;
    }

    private Gson gson = new GsonBuilder().create();

    private Call<ObjectsResult> call;

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.vk.com/") // Адрес сервера
            .addConverterFactory(GsonConverterFactory.create()) // говорим ретрофиту что для сериализации необходимо использовать GSON
            .build();

    @Override
    public void getPhotosViewItems(String text, final PhotoCallback callback) {

        if (text != null) {
            PicassoTools.clearCache(Picasso.with(context));
            cleanDataBase();
            items = new ArrayList<>();
            searchText = text;
        }

        createRequest(countOffSet());

        call.enqueue(new Callback<ObjectsResult>() {
            @Override
            public void onResponse(Call<ObjectsResult> call, Response<ObjectsResult> response) {
                if (response.isSuccessful() && response.body() != null) {

                    photoResponse = (response.body()).result;


                    if (photoResponse != null) {
                        for (PhotoItem item : photoResponse.getItems()) {
                            loadPhotosViewItem(item);
                        }

                        addToDataBase(photoResponse.getItems());
                    }

                    callback.done(null, items, null);
                } else {
                    // сервер вернул ошибку
                    callback.done(new Error("Response is not successful"), null, null);
                }
            }

            @Override
            public void onFailure(Call<ObjectsResult> call, Throwable t) {
                callback.done(new Error(t.getMessage()), null, null);
            }
        });
    }

    @Override
    public void getPhotosFromDataBase(PhotoCallback callback) {
        items = new ArrayList<>();

        ArrayList<PhotoItemRealm> list = getItemsRealmList();

        if (!list.isEmpty()) {
            PhotoItemRealm itemRealm = list.get(list.size() - 1);

            searchText = itemRealm.getText();

            for (PhotoItem item : itemRealm.getPhotoItems()) {
                PhotoViewItem photoViewItem = new PhotoViewItem();

                photoViewItem.url = item.getUrl();
                photoViewItem.height = item.getHeight();
                photoViewItem.width = item.getWidth();

                items.add(photoViewItem);
            }

            callback.done(null, items, searchText);
        }
    }

    @Override
    public ArrayList<PhotoViewItem> getItems() {
        return items;
    }

    //Private

    private void createRequest(int countOffSet) {
        PhotosApi photosApi = retrofit.create(PhotosApi.class);
        call = photosApi.listPhotos(searchText, countOffSet);
    }

    private void loadPhotosViewItem(PhotoItem photoItem) {
        PhotoViewItem item = new PhotoViewItem();

        item.url = photoItem.getUrl();
        item.width = photoItem.getWidth();
        item.height = photoItem.getHeight();

        items.add(item);
    }

    private ArrayList<PhotoItemRealm> getItemsRealmList() {
        RealmQuery<PhotoItemRealm> query = realm.where(PhotoItemRealm.class);
        RealmResults<PhotoItemRealm> results = query.findAll();

        return (ArrayList<PhotoItemRealm>) realm.copyFromRealm(results);
    }

    private void addToDataBase(RealmList<PhotoItem> photoItems) {
        PhotoItemRealm photoItemRealm = new PhotoItemRealm();

        photoItemRealm.setText(searchText);
        photoItemRealm.setPhotoItems(photoItems);

        realm.beginTransaction();

        if (getItemsRealmList().size() != 0) {
            realm.copyToRealm(photoItemRealm);

        } else {
            PhotoItemRealm newPhotoItemRealm = realm.createObject(PhotoItemRealm.class);

            RealmList<PhotoItem> items = newPhotoItemRealm.getPhotoItems();

            items.addAll(photoItems);

            newPhotoItemRealm.setText(searchText);
            newPhotoItemRealm.setPhotoItems(items);
        }

        realm.commitTransaction();
    }

    private void cleanDataBase() {
        realm.beginTransaction();

        RealmQuery<PhotoItemRealm> query = realm.where(PhotoItemRealm.class);
        RealmResults<PhotoItemRealm> results = query.findAll();

        results.deleteAllFromRealm();

        realm.commitTransaction();
    }

    private int countOffSet() {
        if (items == null) {
            return 0;
        }

        return items.size();
    }
}