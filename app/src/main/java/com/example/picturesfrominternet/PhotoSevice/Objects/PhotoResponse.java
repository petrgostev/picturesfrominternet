package com.example.picturesfrominternet.PhotoSevice.Objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;


public class PhotoResponse extends RealmObject {

    @SerializedName("count")
    private Integer count;

    @SerializedName("items")
    private RealmList<PhotoItem> items = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public RealmList<PhotoItem> getItems() {
        return items;
    }

    public void setItems(RealmList<PhotoItem> items) {
        this.items = items;
    }
}
