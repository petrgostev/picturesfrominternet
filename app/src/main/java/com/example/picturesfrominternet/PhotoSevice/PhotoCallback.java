package com.example.picturesfrominternet.PhotoSevice;

import com.example.picturesfrominternet.PhotosList.Interactor.PhotoViewItem;

import java.util.ArrayList;


public interface PhotoCallback {
    void done(Error error, ArrayList<PhotoViewItem> photos, String text);
}
